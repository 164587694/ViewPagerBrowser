package com.reone.viewpagerbrowser;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.reone.viewpagerbrowser.view.ViewsPagerBrowser;

import java.util.ArrayList;


public class BrowserViewPagerActivity extends ActionBarActivity {

    private ViewsPagerBrowser mViewPagerBrowser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser_view_pager);
        mViewPagerBrowser = (ViewsPagerBrowser) findViewById(R.id.views_pager_browser);
        mViewPagerBrowser.setCanBecircle(true);
        ArrayList<View> views = getViews();
        mViewPagerBrowser.initOrUpdateViews(views);
        mViewPagerBrowser.setItemClickListner(new ViewsPagerBrowser.OnBrowserViewPagerItemClickListner(){

            @Override
            public void onItemClickListner(View itemview, int pos) {
                Toast.makeText(BrowserViewPagerActivity.this,"pos = " + pos, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private ArrayList<View> getViews(){
        ArrayList<View> views = new ArrayList<>();
        ImageView iv1 = new ImageView(getApplicationContext());
        iv1.setImageResource(R.drawable.user_background_1);
        ImageView iv2 = new ImageView(getApplicationContext());
        iv2.setImageResource(R.drawable.user_background_2);
        ImageView iv3 = new ImageView(getApplicationContext());
        iv3.setImageResource(R.drawable.user_background_3);
        views.add(iv1);
        views.add(iv2);
        views.add(iv3);
        return views;
    }
}
